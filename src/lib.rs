#[macro_use]
extern crate lazy_static;

pub mod digestion;
pub mod protein;


const LIMIT_PEPTIDE_LENGHT: u8 = 255;