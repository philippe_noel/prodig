extern crate prolib;
use prolib::protein::peptide;
use prolib::digestion::dig;

fn main() {
    let my_pep = peptide::Peptide::new(String::from("MNGTEGPNFYVPFSNATGVVRSPFEYPQYYLAEPWQFSMLAAYMFLLIVLGFPINFLTLYVTVQHKKLRTPLNYILLNLAVADLFMVLGGFTSTLYTSLHGYFVFGPTGCNLEGFFATLGGEIALWSLVVLAIERYVVVCKPMSNFRFGENHAIMGVAFTWVMALACAAPPLAGWSRYIPEGLQCSCGIDYYTLKPEVNNESFVIYMFVVHFTIPMIIIFFCYGQLVFTVKEAAAQQQESATTQKAEKEVTRMVIIMVIAFLICWVPYASVAFYIFTHQGSNFGPIFMTIPAFFAKSAAIYNPVIYIMMNKQFRNCMLTTICCGKNPLGDDEASATVSKTETSQVAPA"));
    println!("weight : {}", my_pep.weight);
    let my_frag = dig::tryp_digest(&my_pep);
    
    println!("len {}", my_frag.len());
    for ppt in my_frag {
        println!("{} : {}", ppt.seq, ppt.weight);
    }

}
