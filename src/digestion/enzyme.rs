/// Create a digest `Enzyme`
enum Enzyme {
    Lys_c,
    Trypsin,
    Arg_c,
    Asp_n,
    V8_bicarb,
    V8_phosph,
    Chymotrypsin,
    CNBr,
}

impl Enzyme {
    fn get_site(&self) -> Vec<char> {
        use self::Enzyme::*;

        let mut sites: Vec<char> = Vec::new();

        match self {
            Lys_c        => sites.push('A'),
            Trypsin      => sites.push('R'),
            Arg_c        => sites.push('R'),
            Asp_n        => sites.push('R'),
            V8_bicarb    => sites.push('R'),
            V8_phosph    => sites.push('R'),
            Chymotrypsin => sites.push('R'),
            CNBr         =>  sites.push('R'),
        }
        sites
    }
}