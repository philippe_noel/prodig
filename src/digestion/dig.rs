use crate::protein::peptide::Peptide;

/// Function which digest a `Peptide` and return a vector of `Peptide structure`
/// 
/// # Examples
/// ```
/// extern crate prolib;
/// use prolib::protein::peptide;
/// use prolib::digestion::dig;
/// let my_pep = peptide::Peptide::new(String::from("ATGCILRMNDSDSKQDSQD"));
///     let my_frag = dig::tryp_digest(&my_pep);
///     assert_eq!(2, my_frag.len());
/// ```
pub fn tryp_digest(p: &Peptide) -> Vec<Peptide> {

    let mut lst_frag: Vec<Peptide> = Vec::new();

    let mut tmp_index: usize = 0;

    for (index, aa) in p.seq.chars().enumerate() {
        if aa == 'R' || aa == 'K' && index + 1 < p.lenght {
            lst_frag.push(
                Peptide::new(format!("{}", &(p.seq[tmp_index..index+1])))
            );
            tmp_index = index+1;
        }
    }
    // to get the last fragment
    lst_frag.push(
        Peptide::new(format!("{}", &(p.seq[tmp_index..p.lenght])))
    );
    lst_frag
}