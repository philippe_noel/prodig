use crate::protein::amino_acid;

pub struct Peptide {
    pub seq: String,
    pub weight: f32,
    pub lenght: usize,
}

impl Peptide {
    pub fn new(seq: String) -> Peptide {
        Peptide {
            weight: Peptide::compute_weight(&seq),
            lenght: seq.as_bytes().len(),
            seq: String::from(seq),
        }
    }

    pub fn compute_weight(seq: &String) -> f32 {
        let mut total = 0.0;
        for ii in seq.chars() {
            total += amino_acid::size_amino_acid(ii);
        }
        total += 17.00738; // C-terminus (OH)
        total += 1.00797; // N-terminus (H)
        total
    }
}
