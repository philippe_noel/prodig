use std::collections::HashMap;

lazy_static! {
    //https://pepcalc.com/
    static ref residue_weight: HashMap<char, f32> = [
        ('A', 71.07793), ('C', 103.1454), ('D', 115.0873), ('E', 129.1139),
        ('F', 147.1734), ('G', 57.05138), ('H', 137.1394), ('I', 113.1576),
        ('K', 128.1724), ('L', 113.1576), ('M', 131.1985), ('N', 114.1028),
        ('P', 97.11508), ('Q', 128.1293), ('R', 156.1861), ('S', 87.07733),
        ('T', 101.1039), ('V', 99.13103), ('W', 186.2095), ('Y', 163.1728),
    ].iter()
        .cloned()
        .collect();
}

pub fn size_amino_acid(aa: char) -> f32 {
    residue_weight[&aa]
}